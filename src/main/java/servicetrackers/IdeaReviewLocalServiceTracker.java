package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import ideaService.service.ReviewLocalService;

public class IdeaReviewLocalServiceTracker extends ServiceTracker<ReviewLocalService, ReviewLocalService> {

    public IdeaReviewLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, ReviewLocalService.class, null);
    }
}
