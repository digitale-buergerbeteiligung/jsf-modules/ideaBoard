package beans;

import analysisService.service.SessionTimeLocalService;
import analysisService.service.TagDataLocalService;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.faces.util.logging.Logger;
import com.liferay.faces.util.logging.LoggerFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.*;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.*;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import enums.DisplayStyle;
import ideaService.model.Ideas;
import ideaService.service.IdeasLocalService;
import ideasService.service.enums.ReviewStatus;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import projectService.service.ProjectLocalService;
import servicetrackers.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.portlet.PortletRequest;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

@ManagedBean
@ViewScoped
public class IdeaInputBean implements Serializable {

	private static final long serialVersionUID = -6014998996217013586L;
	private final String IDEAS_PICTURE_FOLDER = "IdeasPictureFolder";
	private final String IDEAS_VIDEO_FOLDER = "IdeasVideoFolder";
	private final String IDEAS_PICTURE_QUALIFIER = "IDEAS_PICTURE";
	private final String IDEAS_VIDEO_QUALIFIER = "IDEAS_VIDEO";
	private final String IDEA_BOARD = "IdeaBoard";

	private final String FIRST_BUTTON_CONTINUE = "First_Button_Continue";
	private final String SECOND_BUTTON_CONTINUE = "Second_Button_Continue";
	private final String THIRD_BUTTON_CONTINUE = "Third_Button_Continue";
	private final String FINAL_BUTTON_SAVE = "Final_Button_Save";
	private final String FINAL_BUTTON_SUBMIT = "Final_Button_Submit";

	private static Logger logger;

	@ManagedProperty(value = "#{ideasBean}")
	private IdeasBean ideasBean;

	public void setIdeasBean(IdeasBean ideasBean) {
		this.ideasBean = ideasBean;
	}

	private String title;
	private String description;
	private String shortDescription;
	private String importance;
	private String solution;
	private String targetAudience;
	private String tags = "";
	private String goal;
	private String selectedCategory;
	private boolean publish;
	private double lat;
	private double lng;
	private List<String> selectedTags;
	private long currentIdeasId;
	private String dataUrl1;
	private String dataUrl2;
	private String dataUrl3;
	private String pitch;		
	private boolean privacyNoticeCheckbox;					// must be checked by user in order to proceed to step 3 of idea creation				
	// solely exists to show the img in the last step.
	private String titleImgUrl1 = "/";
	private String titleImgUrl2 = "/";
	private String titleImgUrl3 = "/";
	private static final String ANONYM = "Anonym";
	// solely exists to show the video in the last step.
	private String videoPreviewUrl;
	//This is for the dropdown selection of the username/ full name / anonymous
	private String selectedUserName;
	private Map<String,String> possibleUsernames;


	private LayoutLocalServiceTracker layoutLocalServiceTracker;
	private IdeaLocalServiceTracker ideasLocalServiceTracker;
	private UserLocalServiceTracker userLocalServiceTracker;
	private DLFileEntryLocalServiceTracker dlfileEntryLocalServiceTracker;
	private DLFolderLocalServiceTracker dlfolderLocalServiceTracker;
	private DLAppLocalServiceTracker dlAppLocalServiceTracker;
	private ProjectLocalServiceTracker projectLocalServiceTracker;
	private FriendlyUrlLocalServiceTracker friendlyURLLocalSerivceTracker;
	private CategoryLocalServiceTracker categoryLocalServiceTracker;
	private SessionTimeLocalServiceTracker sessionTimeTracker;
	private TagLocalServiceTracker tagLocalServiceTracker;
	private RoleLocalServiceTracker roleLocalServiceTracker;

	private UploadedFile ideasPicture;
	private UploadedFile ideasVideo;
	private InputStream ideasPicInputStream;
	private InputStream ideasVideoInputStream;

	private String styleModalOne = DisplayStyle.BLOCK.getDisplayStyleString();
	private String styleModalTwo = DisplayStyle.NONE.getDisplayStyleString();
	private String styleModalThree = DisplayStyle.NONE.getDisplayStyleString();
	private String styleModalOverview = DisplayStyle.NONE.getDisplayStyleString();
	
	private boolean isUserLoggedIn = false;
	
	private boolean stepTwoVisited = false;
	
	private boolean onLoadIdea = false;
	
	
	@PostConstruct
	public void postConstruct() {		
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		BundleContext bundleContext = bundle.getBundleContext();
		ideasLocalServiceTracker = new IdeaLocalServiceTracker(bundleContext);
		userLocalServiceTracker = new UserLocalServiceTracker(bundleContext);
		dlfileEntryLocalServiceTracker = new DLFileEntryLocalServiceTracker(bundleContext);
		dlfolderLocalServiceTracker = new DLFolderLocalServiceTracker(bundleContext);
		dlAppLocalServiceTracker = new DLAppLocalServiceTracker(bundleContext);
		projectLocalServiceTracker = new ProjectLocalServiceTracker(bundleContext);
		layoutLocalServiceTracker = new LayoutLocalServiceTracker(bundleContext);
		friendlyURLLocalSerivceTracker = new FriendlyUrlLocalServiceTracker(bundleContext);
		categoryLocalServiceTracker = new CategoryLocalServiceTracker(bundleContext);
		sessionTimeTracker = new SessionTimeLocalServiceTracker(bundleContext);
		tagLocalServiceTracker = new TagLocalServiceTracker(bundleContext);
		roleLocalServiceTracker = new RoleLocalServiceTracker(bundleContext);
		categoryLocalServiceTracker.open();
		projectLocalServiceTracker.open();
		dlAppLocalServiceTracker.open();
		dlfolderLocalServiceTracker.open();
		dlfileEntryLocalServiceTracker.open();
		ideasLocalServiceTracker.open();
		userLocalServiceTracker.open();
		layoutLocalServiceTracker.open();
		friendlyURLLocalSerivceTracker.open();
		sessionTimeTracker.open();
		tagLocalServiceTracker.open();
		roleLocalServiceTracker.open();

		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebKeys.THEME_DISPLAY);
		
		logger = LoggerFactory.getLogger(this.getClass().getName());		
		
		if (themeDisplay.isSignedIn()) {
			UserLocalService userLocalService = userLocalServiceTracker.getService();
			User user = null;
			try {
				user = userLocalService.getUserById(
						Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser()));
			} catch (NumberFormatException | PortalException e) {
				logger.info("Could not find username or user is not logged in.");
			}
			possibleUsernames = new HashMap<String, String>();
			String fullName = user.getFullName();
			if (fullName.length() > 30) {
				fullName = fullName.substring(0, 30);
			}
			possibleUsernames.put(fullName, fullName);

			String screenName = user.getScreenName();
			if (screenName.length() > 30) {
				screenName = screenName.substring(0, 30);
			}
			possibleUsernames.put(screenName, screenName);
			possibleUsernames.put(ANONYM, ANONYM);
		
			this.isUserLoggedIn = true;
//			updateMainBody();
			
		}


		logger.info(this.getClass().getName() + " initialized successfully");
		// The JSF version
		// logger.info(FacesContext.class.getPackage().getImplementationVersion());
		 updateMainBody();
	}

	public boolean getIsUserLoggedIn() {
		return this.isUserLoggedIn;
	}

	public void setIsUserLoggedIn(boolean isUserLoggedIn) {
		this.isUserLoggedIn = isUserLoggedIn;
	}
	
	public boolean getStepTwoVisited() {
		return this.stepTwoVisited;
	}
	
	public void setstepTwoVisited(boolean stepTwoVisited) {
		this.stepTwoVisited = stepTwoVisited;
	}

	@PreDestroy
	public void preDestroy() {
		ideasLocalServiceTracker.close();
		userLocalServiceTracker.close();
		dlfileEntryLocalServiceTracker.close();
		dlfolderLocalServiceTracker.close();
		dlAppLocalServiceTracker.close();
		projectLocalServiceTracker.close();
		layoutLocalServiceTracker.close();
		friendlyURLLocalSerivceTracker.close();
		categoryLocalServiceTracker.close();
		sessionTimeTracker.close();
		tagLocalServiceTracker.close();
		roleLocalServiceTracker.close();
	}

	public void onPageLoad() {
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		// forces the user to the login page even on page refresh or going back through the back button.
		if (!themeDisplay.isSignedIn()) {
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideainputform");
		}

		if (themeDisplay.isSignedIn())
		if(themeDisplay.getURLCurrent().contains("update")){
			String [] split = themeDisplay.getURLCurrent().split("=");
			loadIdea(Long.parseLong(split[1]));
		}
	}

	private void loadIdea(long ideasId){
		this.onLoadIdea = true;
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		UserLocalService userLocalService = userLocalServiceTracker.getService();

		try {
			Ideas idea = ideasLocalService.getIdeas(ideasId);

			nullfyInputFields();
			this.currentIdeasId = idea.getPrimaryKey();
			this.selectedTags = ideasLocalService.getTagsForIdea(ideasId);
			this.title = idea.getTitle();
			this.solution = idea.getSolution();
			this.goal = idea.getGoal();
			this.importance = idea.getImportance();
			this.targetAudience = idea.getTargetAudience();
			this.lat = idea.getLatitude();
			this.lng = idea.getLongitude();
			this.pitch = idea.getPitch();			
			this.titleImgUrl1 = ideasLocalService.getPictureUrlByIdeasRefAndPosition(ideasId, 0);


			this.selectedUserName = idea.getUserName();

			// Load the Video if available
			if (idea.getVideoUrl() != null && !idea.getVideoUrl().equals("")) {
				String[] split = idea.getVideoUrl().split(",");
				if (split.length > 1) {
					for (int j = 0; j < split.length; j++) {
						if (FilenameUtils.getExtension(split[j]).equals("mp4")) {
							this.videoPreviewUrl = split[j];
						}
					}
				} else {
					this.videoPreviewUrl = idea.getVideoUrl();
				}
			}

			this.titleImgUrl2 = ideasLocalService.getPictureUrlByIdeasRefAndPosition(ideasId, 1);
			
			this.titleImgUrl3 = ideasLocalService.getPictureUrlByIdeasRefAndPosition(ideasId, 2);

			updateMainBody();
			logger.info("Loaded Idea with primary key " + ideasId + " successfully");

		} catch (PortalException e) {
			logger.error("Could not load Idea from DB", e);
		}
	}

	private void updateMainBody(){
		try {
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:mainBody");
		} catch (Exception ex) {
			logger.error("Could not update main body",ex);
		}
	}

	/**
	 * first step allows the user to enter textual information
	 *
	 * @return created ideas id
	 * @throws NumberFormatException
	 * @throws PortalException
	 */
	public void ideaInputStepOne() {
		setStyleModalOne(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.BLOCK.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.NONE.getDisplayStyleString());
		updateMainBody();
		trackClick(FIRST_BUTTON_CONTINUE);
		
		this.stepTwoVisited = true;
		
		if (this.currentIdeasId > 0) {
			try {
				updateIdea();
				return;
			} catch (PortalException | IOException e) {
				logger.error("Could not update idea", e);
			}
		}

		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ProjectLocalService projectLocalService = projectLocalServiceTracker.getService();
		UserLocalService userLocalService = userLocalServiceTracker.getService();

		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		User user = null;
		try {
			user = userLocalService.getUserById(
					Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser()));
		} catch (NumberFormatException | PortalException e1) {
			logger.error("Could not find user", e1);
		}

		boolean isVisibleOnMap = false;
		if (Double.isNaN(this.getLat()) || Double.isNaN(this.getLng())) {
			this.setLat(0.0);
			this.setLng(0.0);
		}
		if (this.getLat() != 0.0 && this.getLng() != 0.0) {
			isVisibleOnMap = true;
		}

		Layout page = null;
		try {
			//Create new project
			long projectRef = 0L;
			try {
				projectRef = projectLocalService.getProjectByLayoutIdRef(themeDisplay.getLayout().getPrimaryKey())
						.getPrimaryKey();
			} catch (Exception e) {
				logger.info("Could not find projectRef. Attaching Idea to Project with ID 0.");
			}

			// gets the primary keys of the added tags
			if (!(selectedTags == null) && !selectedTags.isEmpty()) {
				tags = getTagsAsPrimaryKeyList().toString();
				tags = tags.substring(1, tags.length() - 1);
			}

			Ideas nextIdea = ideasLocalService.createIdeasWithAutomatedDbId(user.getUserId(), projectRef,
					themeDisplay.getScopeGroupId(), 0L, title, solution, description, importance,
					targetAudience, tags, goal, "", lat, lng, ReviewStatus.SAVED, isVisibleOnMap);

			currentIdeasId = nextIdea.getPrimaryKey();

			logger.info("Created new idea with primary key " + nextIdea.getPrimaryKey() + " by user "
					+ nextIdea.getUserName() + "(" + nextIdea.getUserId() + ")");

			page = createLayoutForIdea(themeDisplay, user, isVisibleOnMap, false, true);
			nextIdea.setLayoutRef(page.getPrimaryKey());
			nextIdea.setPageUrl(page.getFriendlyURL());
			ideasLocalService.persistIdeasAndPerformTypeChecks(nextIdea);
		} catch (PortalException e1) {
			try {
				// In case the layout already exists the Idea is simply updated
				// and no new Idea or Layout are created.
				this.updateIdea();
				return; // skip rest of step if it's an update.
			} catch (PortalException | IOException e) {
				logger.error("Could not create or update the idea!", e);
				return;
			}
		}

	}

	/**
	 * first step allows the user to enter textual information
	 *
	 * @return created ideas id
	 * @throws NumberFormatException
	 * @throws PortalException
	 */
	public void ideaInputStepOneToStepThree() {
		setStyleModalOne(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.BLOCK.getDisplayStyleString());
		updateMainBody();
		trackClick(FIRST_BUTTON_CONTINUE);
		if (this.currentIdeasId > 0) {
			try {
				updateIdea();
				return;
			} catch (PortalException | IOException e) {
				logger.error("Could not update idea", e);
			}
		}

		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ProjectLocalService projectLocalService = projectLocalServiceTracker.getService();
		UserLocalService userLocalService = userLocalServiceTracker.getService();

		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		User user = null;
		try {
			user = userLocalService.getUserById(
					Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser()));
		} catch (NumberFormatException | PortalException e1) {
			logger.error("Could not find user", e1);
		}

		boolean isVisibleOnMap = false;
		if (Double.isNaN(this.getLat()) || Double.isNaN(this.getLng())) {
			this.setLat(0.0);
			this.setLng(0.0);
		}
		if (this.getLat() != 0.0 && this.getLng() != 0.0) {
			isVisibleOnMap = true;
		}

		Layout page = null;
		try {
			//Create new project
			long projectRef = 0L;
			try {
				projectRef = projectLocalService.getProjectByLayoutIdRef(themeDisplay.getLayout().getPrimaryKey())
						.getPrimaryKey();
			} catch (Exception e) {
				logger.info("Could not find projectRef. Attaching Idea to Project with ID 0.");
			}

			// gets the primary keys of the added tags
			if (!(selectedTags == null) && !selectedTags.isEmpty()) {
				tags = getTagsAsPrimaryKeyList().toString();
				tags = tags.substring(1, tags.length() - 1);
			}

			Ideas nextIdea = ideasLocalService.createIdeasWithAutomatedDbId(user.getUserId(), projectRef,
					themeDisplay.getScopeGroupId(), 0L, title, solution, description, importance,
					targetAudience, tags, goal, "", lat, lng, ReviewStatus.SAVED, isVisibleOnMap);

			currentIdeasId = nextIdea.getPrimaryKey();

			logger.info("Created new idea with primary key " + nextIdea.getPrimaryKey() + " by user "
					+ nextIdea.getUserName() + "(" + nextIdea.getUserId() + ")");

			page = createLayoutForIdea(themeDisplay, user, isVisibleOnMap, false, true);
			nextIdea.setLayoutRef(page.getPrimaryKey());
			nextIdea.setPageUrl(page.getFriendlyURL());
			ideasLocalService.persistIdeasAndPerformTypeChecks(nextIdea);
		} catch (PortalException e1) {
			try {
				// In case the layout already exists the Idea is simply updated
				// and no new Idea or Layout are created.
				this.updateIdea();
				return; // skip rest of step if it's an update.
			} catch (PortalException | IOException e) {
				logger.error("Could not create or update the idea!", e);
				return;
			}
		}

	}
	
	public void backToStepOne(){
		setStyleModalOne(DisplayStyle.BLOCK.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.NONE.getDisplayStyleString());
		updateMainBody();
	}

	/**
	 * the user can upload a picture in this step
	 */
	public void ideaInputStepTwo() {		
		setStyleModalOne(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.BLOCK.getDisplayStyleString());
		updateMainBody();
		
		trackClick(SECOND_BUTTON_CONTINUE);

		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		File tmpFile = null;
		if(this.dataUrl1.equals("-1")) {
			this.titleImgUrl1 = "/";
			//delete picture from idea
			long pictureId = ideasLocalService.getPictureIdByIdeasRefAndPosition(currentIdeasId, 0);
			if(pictureId != 0) {
				logger.info("Picture " + Long.toString(pictureId) + " from idea " + currentIdeasId + " deleted");			
				ideasLocalService.deletePictureById(pictureId);
			}
		} else { 
		
			if (this.dataUrl1 != null && !this.dataUrl1.equals("")) {
				
				try {
					tmpFile = convertDataUrlToFile(this.dataUrl1);
				} catch (IOException e1) {
					return;
				}			
				
				FileEntry createdEntry = null;
				if (tmpFile != null && dataUrl1 != null) {
					try {
						createdEntry = saveFile(tmpFile, this.IDEAS_PICTURE_FOLDER, this.IDEAS_PICTURE_QUALIFIER);
					} catch (PortalException | IOException e) {
						logger.error("Could not upload a picture", e);
					}
					String titleImgRef = getFullyQualifiedUrlForFileEntry(themeDisplay, createdEntry);
					titleImgUrl1 = titleImgRef;
					long picPrimaryKey = createdEntry.getPrimaryKey();
					ideasLocalService.addPictureToExistingIdea(currentIdeasId, picPrimaryKey, titleImgRef,0);				
					logger.info("Added Picture to Idea with primary key " + currentIdeasId);
				}
			}
		}
		
		if(this.dataUrl2.equals("-1")) {
			this.titleImgUrl2 = "/";
			//delete picture from idea
			long pictureId = ideasLocalService.getPictureIdByIdeasRefAndPosition(currentIdeasId, 1);
			if(pictureId != 0) {
				logger.info("Picture " + Long.toString(pictureId) + " from idea " + currentIdeasId + " deleted");			
				ideasLocalService.deletePictureById(pictureId);
			} 
		}else {
			if(this.dataUrl2 != null && !this.dataUrl2.equals("")){
				this.titleImgUrl2 = addAdditionalPicture(this.dataUrl2,themeDisplay,1);
			}
		}
		
		
		if(this.dataUrl3.equals("-1")) {
			this.titleImgUrl3 = "/";
			//delete picture from idea
			long pictureId = ideasLocalService.getPictureIdByIdeasRefAndPosition(currentIdeasId, 2);
			if(pictureId != 0) {
				logger.info("Picture " + Long.toString(pictureId) + " from idea " + currentIdeasId + " deleted");			
				ideasLocalService.deletePictureById(pictureId);
			} 
		} else {
			if(this.dataUrl3 != null && !this.dataUrl3.equals("")){
				this.titleImgUrl3 = addAdditionalPicture(this.dataUrl3,themeDisplay,2);
			}
		}
		
		RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:mainBodyOverview");
		if(tmpFile != null)
			tmpFile.delete();
	}
	/**
	 * the user can upload a picture in this step
	 */
	public void ideaInputStepTwoToStepOne() {	
		setStyleModalOne(DisplayStyle.BLOCK.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.NONE.getDisplayStyleString());
		updateMainBody();
		
		trackClick(SECOND_BUTTON_CONTINUE);

		if(this.dataUrl1 == null || this.dataUrl1.equals("")){			
			return;
		}
		
		File tmpFile = null;
		try {
			tmpFile = convertDataUrlToFile(this.dataUrl1);
		} catch (IOException e1) {
			return;
		}

		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		
		FileEntry createdEntry = null;
		if (tmpFile != null && dataUrl1 != null) {
			try {
				createdEntry = saveFile(tmpFile, this.IDEAS_PICTURE_FOLDER, this.IDEAS_PICTURE_QUALIFIER);
			} catch (PortalException | IOException e) {
				logger.error("Could not upload a picture", e);
			}
			String titleImgRef = getFullyQualifiedUrlForFileEntry(themeDisplay, createdEntry);
			titleImgUrl1 = titleImgRef;
			long picPrimaryKey = createdEntry.getPrimaryKey();
			ideasLocalService.addPictureToExistingIdea(currentIdeasId, picPrimaryKey, titleImgRef,0);
			logger.info("Added Picture to Idea with primary key " + currentIdeasId);
		}
		
		if(this.dataUrl2 != null && !this.dataUrl2.equals("")){
			this.titleImgUrl2 = addAdditionalPicture(this.dataUrl2,themeDisplay,1);
		}
		
		if(this.dataUrl3 != null && !this.dataUrl3.equals("")){
			this.titleImgUrl3 = addAdditionalPicture(this.dataUrl3,themeDisplay,2);
		}
		
		RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:mainBodyOverview");
		tmpFile.delete();
	}
	
	public void backToStepTwo(){
		setStyleModalOne(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.BLOCK.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.NONE.getDisplayStyleString());
		updateMainBody();
	}


	/**
	 * the user can upload a video in this step
	 */
	public void ideaInputStepThree() {
		setStyleModalOne(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalOverview(DisplayStyle.BLOCK.getDisplayStyleString());

		trackClick(THIRD_BUTTON_CONTINUE);
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		// save the ideas video
		FileEntry createdVideoEntry = null;
		if (this.ideasVideo != null) {
			try {
				this.ideasVideoInputStream = ideasVideo.getInputstream();
				createdVideoEntry = saveFile(this.ideasVideo, this.IDEAS_VIDEO_FOLDER, this.IDEAS_VIDEO_QUALIFIER);
			} catch (PortalException | IOException e) {
				logger.error("Could not upload a video", e);
			}
			String videoRef = getFullyQualifiedUrlForFileEntry(themeDisplay, createdVideoEntry);
			this.videoPreviewUrl = videoRef;
			long videoPrimaryKey = createdVideoEntry.getPrimaryKey();
			ideasLocalService.addVideoToExistingIdea(currentIdeasId, videoPrimaryKey, videoRef, themeDisplay);
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputOverviewForm");
			logger.info("Added Video to Idea with primary key " + currentIdeasId);
		}

		if (this.pitch != null && this.pitch != "") {
			ideasLocalService.addElevatorPitchToExisitingIdea(currentIdeasId, pitch);
		}
		this.ideasVideo = null;
		updateAllInputFields("ideaInputOverviewForm");
	}

	public void ideaInputStepThreeToStepTwo() {
		setStyleModalOne(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.BLOCK.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.NONE.getDisplayStyleString());

		trackClick(THIRD_BUTTON_CONTINUE);
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		// save the ideas video
		FileEntry createdVideoEntry = null;
		if (this.ideasVideo != null) {
			try {
				this.ideasVideoInputStream = ideasVideo.getInputstream();
				createdVideoEntry = saveFile(this.ideasVideo, this.IDEAS_VIDEO_FOLDER, this.IDEAS_VIDEO_QUALIFIER);
			} catch (PortalException | IOException e) {
				logger.error("Could not upload a video", e);
			}
			String videoRef = getFullyQualifiedUrlForFileEntry(themeDisplay, createdVideoEntry);
			this.videoPreviewUrl = videoRef;
			long videoPrimaryKey = createdVideoEntry.getPrimaryKey();
			ideasLocalService.addVideoToExistingIdea(currentIdeasId, videoPrimaryKey, videoRef, themeDisplay);
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputOverviewForm");
			logger.info("Added Video to Idea with primary key " + currentIdeasId);
		}

		if (this.pitch != null && this.pitch != "") {
			ideasLocalService.addElevatorPitchToExisitingIdea(currentIdeasId, pitch);
		}
		this.ideasVideo = null;
		updateMainBody();
	}
	
	public void ideaInputStepThreeToStepOne() {
		setStyleModalOne(DisplayStyle.BLOCK.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.NONE.getDisplayStyleString());

		trackClick(THIRD_BUTTON_CONTINUE);
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		// save the ideas video
		FileEntry createdVideoEntry = null;
		if (this.ideasVideo != null) {
			try {
				this.ideasVideoInputStream = ideasVideo.getInputstream();
				createdVideoEntry = saveFile(this.ideasVideo, this.IDEAS_VIDEO_FOLDER, this.IDEAS_VIDEO_QUALIFIER);
			} catch (PortalException | IOException e) {
				logger.error("Could not upload a video", e);
			}
			String videoRef = getFullyQualifiedUrlForFileEntry(themeDisplay, createdVideoEntry);
			this.videoPreviewUrl = videoRef;
			long videoPrimaryKey = createdVideoEntry.getPrimaryKey();
			ideasLocalService.addVideoToExistingIdea(currentIdeasId, videoPrimaryKey, videoRef, themeDisplay);
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputOverviewForm");
			logger.info("Added Video to Idea with primary key " + currentIdeasId);
		}

		if (this.pitch != null && this.pitch != "") {
			ideasLocalService.addElevatorPitchToExisitingIdea(currentIdeasId, pitch);
		}
		this.ideasVideo = null;
		updateMainBody();
	}
	
	public void backToStepThree(){
		setStyleModalOne(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalTwo(DisplayStyle.NONE.getDisplayStyleString());
		setStyleModalThree(DisplayStyle.BLOCK.getDisplayStyleString());
		setStyleModalOverview(DisplayStyle.NONE.getDisplayStyleString());
		updateMainBody();
	}

	/**
	 * function called when an idea is saved and only visible for the creator of
	 * the idea
	 */
	public void ideaInputFinalSave() {
		try {
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:mainBodyOverview");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		trackClick(FINAL_BUTTON_SAVE);
		try {
			updateIdea();
			logger.info("Saved Idea with primary key " + currentIdeasId);
		} catch (PortalException | IOException e) {
			logger.error("Could not save Idea to DB", e);
		}
		this.titleImgUrl1 = "/";
		this.titleImgUrl2 = "/";
		this.titleImgUrl3 = "/";
		this.ideasPicture = null;
		this.dataUrl1 = null;
		this.dataUrl2 = null;
		this.dataUrl3 = null;
		this.ideasVideoInputStream = null;

		//Redirect
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
			try {
				externalContext.redirect("/web/guest/meineprojekte");
			} catch (IOException e) {
				try {
					externalContext.redirect("/web/guest/home");
				} catch (IOException e1) {
					logger.error("Could not redirect" , e1);
				}
			}
	}

	/**
	 * function called when an Idea is submitted in its final form
	 */
	public void ideaInputFinalSubmit() {
		if(!this.privacyNoticeCheckbox) {
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputOverviewForm");
			FacesContext.getCurrentInstance().addMessage("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputOverviewForm:privacyNoticeCheckbox", new FacesMessage(FacesMessage.SEVERITY_ERROR, "","Sie müssen zunächst den AGB und der Datenschutzerklärung zustimmen."));
			return;
		}
		
		try {
			RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:mainBodyOverview");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		trackClick(FINAL_BUTTON_SUBMIT);
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ideasLocalService.setIdeaReviewStatus(currentIdeasId, ReviewStatus.SUBMITTED);
		try {
			updateIdea();
			logger.info("Submitted Idea with primary key " + currentIdeasId);
		} catch (PortalException | IOException e) {
			logger.error("Could not submit Idea to DB", e);
		}
		this.titleImgUrl1 = "/";
		this.titleImgUrl2 = "/";
		this.titleImgUrl3 = "/";
		this.ideasPicture = null;
		this.dataUrl1 = null;
		this.dataUrl2 = null;
		this.dataUrl3 = null;
		this.ideasVideoInputStream = null;
		this.onLoadIdea = false;
		//Redirect
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
			try {
				externalContext.redirect("/web/guest/umfrage");
			} catch (IOException e) {
				try {
					externalContext.redirect("/web/guest/home");
				} catch (IOException e1) {
					logger.error("Could not redirect" , e1);
				}
			}
	}

	private String addAdditionalPicture(String dUrl, ThemeDisplay themeDisplay, int position) {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		File tmpFile = null;
		try {
			tmpFile = convertDataUrlToFile(dUrl);
		} catch (IOException e1) {
			return "";
		}

		String ref = "";
		FileEntry createdEntry = null;
		if (tmpFile != null && dUrl != null) {
			try {
				createdEntry = saveFile(tmpFile, this.IDEAS_PICTURE_FOLDER, this.IDEAS_PICTURE_QUALIFIER);
			} catch (PortalException | IOException e) {
				logger.error("Could not upload a picture", e);
			}
			ref = getFullyQualifiedUrlForFileEntry(themeDisplay, createdEntry);
			long picPk = createdEntry.getPrimaryKey();
			ideasLocalService.addPictureToExistingIdea(currentIdeasId, picPk, ref,position);
			//ideasLocalService.addAdditionalPictureToExistingIdea(currentIdeasId, picPk, ref);
			logger.info("Added additonal Picture to Idea with primary key " + currentIdeasId);
		}
		tmpFile.delete();
		return ref;
	}

	/**
	 * tracks clicks of the given button name
	 *
	 * @param btnName
	 *            button name
	 */
	private void trackClick(String btnName) {
		SessionTimeLocalService sessionTrackerService = sessionTimeTracker.getService();
		sessionTrackerService.trackClick(IDEA_BOARD, btnName);
	}

	/**
	 * nullifies all input fields and updates the views
	 */
	public void nullfyInputFields() {
		this.currentIdeasId = 0L;
		this.setSelectedTags(null);
		this.title = null;
		this.solution = null;
		this.goal = null;
		this.importance = null;
		this.targetAudience = null;
		this.ideasPicture = null;
		this.ideasVideo = null;
		this.lng = Double.NaN;
		this.lat = Double.NaN;
		this.pitch = null;
		this.videoPreviewUrl = null;
		this.titleImgUrl1 = null;
		this.titleImgUrl2 = null;
		this.titleImgUrl3 = null;
		this.dataUrl1 = null;
		this.dataUrl2 = null;
		this.dataUrl3 = null;
		this.selectedUserName = null;
		this.styleModalOne = DisplayStyle.BLOCK.getDisplayStyleString();
		this.styleModalTwo = DisplayStyle.NONE.getDisplayStyleString();
		this.styleModalOverview = DisplayStyle.BLOCK.getDisplayStyleString();
		updateAllInputFields("ideaInputOverviewForm");
		updateAllInputFields("ideaInputStepOneForm");
		RequestContext.getCurrentInstance().update("ideaInputStepTwoForm");
		RequestContext.getCurrentInstance().update("ideaInputStepThreeForm");
		RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:mainBody");
		logger.info("Updated all input fields of the ideas input process");
	}

	/**
	 * refreshes all input fields of first and last modal
	 *
	 * @param form
	 */
	private void updateAllInputFields(String form) {
		// refreshing each field individually prevents the modal from flickering
		RequestContext.getCurrentInstance().update(form + ":ideaTitleInput");
		RequestContext.getCurrentInstance().update(form + ":ideaGoalInput");
		RequestContext.getCurrentInstance().update(form + ":ideaImportanceInput");
		RequestContext.getCurrentInstance().update(form + ":ideaAudienceInput");
		RequestContext.getCurrentInstance().update(form + ":ideaSolutionInput");
		RequestContext.getCurrentInstance().update(form + ":ideaTagInput");
		RequestContext.getCurrentInstance().update(form + ":ideasId");
		RequestContext.getCurrentInstance().update(form + ":lat");
		RequestContext.getCurrentInstance().update(form + ":lng");
		if (form.equals("ideaInputOverviewForm")) {
			RequestContext.getCurrentInstance().update(form + ":ideaPitchInput");
		}
	}

	/**
	 * updates an idea and all associated layouts and urls
	 *
	 * @throws PortalException
	 * @throws IOException
	 */
	public Ideas updateIdea() throws PortalException, IOException {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		LayoutLocalService layoutLocalService = layoutLocalServiceTracker.getService();
		UserLocalService userLocalService = userLocalServiceTracker.getService();
		LayoutFriendlyURLLocalService urlFriendlyTracker = friendlyURLLocalSerivceTracker.getService();

		boolean updateLayout = false;

		User user = userLocalService
				.getUserById(Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser()));

		// Update the idea
		Ideas idea = ideasLocalService.getIdeas(currentIdeasId);
		String newUrl = createUrlForIdea(idea.getPrimaryKey());
		if(!idea.getTitle().equals(this.title)){
			updateLayout = true;
		}
		Layout layout = layoutLocalService.getLayout(idea.getLayoutRef());
		if (idea.getLatitude() == 0.0 && idea.getLongitude() == 0.0 && this.lat != 0.0 && this.lng != 0.0) {
			// Add IdeasMap if coords were added with this update.
			LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
			layoutTypePortlet.addPortletId(user.getUserId(), "Ideenkarte", "column-1", -1);
			idea.setIsVisibleOnMap(true);
		}

		if (!(selectedTags == null) && !selectedTags.isEmpty()) {
				tags = getTagsAsPrimaryKeyList().toString();
			}

		idea.setTitle(title);
		idea.setGoal(goal);
		idea.setImportance(importance);
		idea.setSolution(solution);
		idea.setTargetAudience(targetAudience);
		if (!(selectedTags == null) && !selectedTags.isEmpty()) {
			idea.setTags(tags.substring(1, tags.length() - 1));
		}
		idea.setDescription(description);
		idea.setShortdescription(shortDescription);
		idea.setLatitude(lat);
		idea.setLongitude(lng);
		idea.setPitch(pitch);
		if(!this.onLoadIdea) {
			idea.setUserName(selectedUserName);
		}
		else {
		}
		idea.setCategory(getCategoryPrimaryKeyForReceivedTitle(selectedCategory));

		// Update the layout if title changed
		if(updateLayout){
		layout.setFriendlyURL(newUrl);
		layout.setTitle(title);
		layout.setName(title);
		layout.setDescription(shortDescription);
		layoutLocalService.updateLayout(layout);

		for (LayoutFriendlyURL l : urlFriendlyTracker.getLayoutFriendlyURLs(0,
				urlFriendlyTracker.getLayoutFriendlyURLsCount())) {
			if (idea.getPageUrl().equals(l.getFriendlyURL())) {
				l.setFriendlyURL(newUrl);
				urlFriendlyTracker.updateLayoutFriendlyURL(l);
			}
		}

		// Set the new friendly url for the idea. This has to be done at the
		// latest possible point to preserve the original url
		// which is used to update the friendlyurl data table.
		idea.setPageUrl(newUrl);
		}
		ideasLocalService.updateIdeas(idea);
		return idea;
	}

	/**
	 * Creates a new page for an submitted idea
	 *
	 * @param themeDisplay
	 * @param user
	 * @param isVisibleOnMap
	 * @param isPrivate
	 * @param hidden
	 * @return the created layout
	 * @throws PortalException
	 */
	private Layout createLayoutForIdea(ThemeDisplay themeDisplay, User user, boolean isVisibleOnMap, boolean isPrivate,
			boolean hidden) throws PortalException {
		LayoutLocalService layoutLocalService = layoutLocalServiceTracker.getService();
		PortletRequest portletRequest = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		ServiceContext serviceContext = ServiceContextFactory.getInstance(Layout.class.getName(), portletRequest);
		// always makes an admin own the site
		Layout page = layoutLocalService.addLayout(getAdmin().getUserId(), themeDisplay.getScopeGroupId(), isPrivate,
				themeDisplay.getLayout().getLayoutId(), title, title, shortDescription, LayoutConstants.TYPE_PORTLET,
				hidden, createUrlForIdea(this.currentIdeasId), serviceContext);

		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) page.getLayoutType();
		layoutTypePortlet.setLayoutTemplateId(user.getUserId(), "1_column");
		layoutTypePortlet.addPortletId(user.getUserId(), "JSFIdeaDetailViewer_WAR_JSFIdeaDetailViewer", "column-1", 0);
		if (isVisibleOnMap) {
			// adds the MapModule to the new page if the idea has coordinates.
			layoutTypePortlet.addPortletId(user.getUserId(), "Ideenkarte", "column-1", -1);
		}
		layoutTypePortlet.addPortletId(user.getUserId(), "comment", "column-1", -1);
		layoutLocalService.updateLayout(page.getGroupId(), page.getPrivateLayout(), page.getLayoutId(),
				page.getTypeSettings());
		logger.info("Created new Page with primary key " + page.getPrimaryKey());
		return page;
	}

	/**
	 * Saves a File by using createFolderOrGetExistingFolder and fileUpload
	 *
	 * @param file
	 * @param folderName
	 * @param fileQualifier
	 * @return created FileEntry
	 * @throws PortalException
	 * @throws IOException
	 */
	private FileEntry saveFile(UploadedFile file, String folderName, String fileQualifier)
			throws PortalException, IOException {
		UserLocalService userService = userLocalServiceTracker.getService();

		File currentFile = convertUploadedFileToFile(file);

		PortletRequest portletRequest = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);

		Folder folder = createFolderOrGetExistingFolder(themeDisplay, portletRequest, folderName, folderName);
		long userId = Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
		User user = userService.getUser(userId);
		String title = user.getScreenName() + fileQualifier + FilenameUtils.removeExtension(file.getFileName())
				+ UUID.randomUUID();
		return fileUpload(currentFile, user, title, folder.getName(), themeDisplay, portletRequest);
	}

	/**
	 * Saves a File by using createFolderOrGetExistingFolder and fileUpload
	 *
	 * @param file
	 * @param folderName
	 * @param fileQualifier
	 * @return created FileEntry
	 * @throws PortalException
	 * @throws IOException
	 */
	private FileEntry saveFile(File file, String folderName, String fileQualifier) throws PortalException, IOException {
		UserLocalService userService = userLocalServiceTracker.getService();

		PortletRequest portletRequest = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);

		Folder folder = createFolderOrGetExistingFolder(themeDisplay, portletRequest, folderName, folderName);
		long userId = Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
		User user = userService.getUser(userId);
		String title = user.getScreenName() + fileQualifier + FilenameUtils.removeExtension(file.getName())
				+ UUID.randomUUID();
		return fileUpload(file, user, title, folder.getName(), themeDisplay, portletRequest);
	}

	/**
	 * creates a new folder or returns an existing one
	 *
	 * @param themeDisplay
	 * @param request
	 * @param name
	 * @param description
	 * @return folder
	 * @throws PortalException
	 */
	private Folder createFolderOrGetExistingFolder(ThemeDisplay themeDisplay, PortletRequest request, String name,
			String description) throws PortalException {
		DLAppLocalService dlappLocalService = dlAppLocalServiceTracker.getService();

		long userId = Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
		long repositoryId = themeDisplay.getScopeGroupId();// repository id is
															// same as groupId
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID; // or
																			// 0

		ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(), request);
		Folder folder;
		try {
			folder = dlappLocalService.addFolder(userId, repositoryId, parentFolderId, name, description,
					serviceContext);
		} catch (Exception e) {
			folder = dlappLocalService.getFolder(repositoryId, parentFolderId, name);
		}
		return folder;
	}

	/**
	 * Saves a File using the appropriate Liferay services
	 *
	 * @param file
	 *            the file
	 * @param user
	 *            user
	 * @param title
	 *            title
	 * @param folderName
	 *            folderName
	 * @param themeDisplay
	 *            theme display
	 * @param request
	 *            request
	 * @return the created file entry
	 */
	public FileEntry fileUpload(File file, User user, String title, String folderName, ThemeDisplay themeDisplay,
			PortletRequest request) {

		// remove all whitespaces from the string
		title = title.replaceAll("\\s+", "");
		
		// replace Umlaute while preserving captial letters
		title = title.replace("ü", "ue").replace("ö", "oe").replace("ä", "ae").replace("ß", "ss");
		
		title  = title .replace("Ü(?=[a-zäöüß ])", "Ue").replace("Ö(?=[a-zäöüß ])", "Oe").replace("Ä(?=[a-zäöüß ])","Ae");

		title = title.replace("Ü", "UE").replace("Ö", "OE").replace("Ä", "AE");

		//replace special characters
		title = title.replaceAll("[\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\+\\|\\'\\;\\,\\?\\:]", "");

		DLAppLocalService dlappLocalService = dlAppLocalServiceTracker.getService();
		long repositoryId = themeDisplay.getScopeGroupId();
		String mimeType = MimeTypesUtil.getContentType(file);
		String description = "File added to a project or an idea.";
		String changeLog = "changes";
		Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		try {
			Folder folder = dlappLocalService.getFolder(themeDisplay.getScopeGroupId(), parentFolderId, folderName);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), request);
			InputStream is = new FileInputStream(file);
			FileEntry createdEntry = dlappLocalService.addFileEntry(user.getUserId(), repositoryId,
					folder.getFolderId(), file.getName(), mimeType, title, description, changeLog, is, file.length(),
					serviceContext);
			if (file != null && file.exists()) {
				file.delete();
			}
			return createdEntry;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Upload of a ideas picture
	 *
	 * @param event
	 * @throws IOException
	 */
	public void upload(FileUploadEvent event) throws IOException {
		this.setIdeasPicture(event.getFile());
		RequestContext.getCurrentInstance().update("ideaInputStepTwoForm");
		logger.info("uploaded a new Picture successfully");
	}

	/**
	 * this event is called when someone uploads a video.
	 *
	 * @param event
	 * @throws IOException
	 */
	public void uploadVideo(FileUploadEvent event) throws IOException {
		logger.info("VIDEO SIZE: --------->" + event.getFile().getSize());
		FacesContext.getCurrentInstance().addMessage("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepThreeForm:videoMsg", new FacesMessage("Successful",  "Ihr Video wurde erfolgreich hochgeladen!") );
		RequestContext.getCurrentInstance().update("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepThreeForm:videoMsg");
		this.setIdeasVideo(event.getFile());
		
	}

	/**
	 * converts a uploadedFile to a normal File
	 *
	 * @param uFile
	 * @return the file to convert
	 * @throws IOException
	 */
	private File convertUploadedFileToFile(UploadedFile uFile) throws IOException {
		// Lray seems to automatically move the file after previewing it so have
		// to use globally saved input stream
		// InputStream is = uFile.getInputstream();
		File result = new File(uFile.getFileName());
		if (ideasPicInputStream != null) {
			FileUtils.copyInputStreamToFile(ideasPicInputStream, result);
			ideasPicInputStream = null;
		} else {
			FileUtils.copyInputStreamToFile(ideasVideoInputStream, result);
			ideasVideoInputStream = null;
		}

		return result;
	}

	/**
	 * gets the primary key of a category from the title of a category
	 *
	 * @param title
	 * @return
	 */
	private long getCategoryPrimaryKeyForReceivedTitle(String title) {
		// for (Category c :ideasBean.getCategories() ){
		// if(c.getCategoryTitle().equals(title)){
		// return c.getPrimaryKey();
		// }
		// }
		return -1L;
	}

	/**
	 * Creates a new friendly url for an idea. Currently title and username.
	 *
	 * @param
	 * @return the friendly url for this idea
	 */
	private String createUrlForIdea(long ideasId) {
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		//String result = themeDisplay.getLayout().getFriendlyURL() + "/" + u.getScreenName() + "/" + title;
		String result = themeDisplay.getLayout().getFriendlyURL() + "/" + title + ideasId;
		// remove all whitespaces from the string
		result = result.replaceAll("\\s+", "");
		// replace Umlaute while preserving captial letters
		result = result.replace("ü", "ue").replace("ö", "oe").replace("ä", "ae").replace("ß", "ss");
		
		result = result.replace("Ü(?=[a-zäöüß ])", "Ue").replace("Ö(?=[a-zäöüß ])", "Oe").replace("Ä(?=[a-zäöüß ])","Ae");

		result = result.replace("Ü", "UE").replace("Ö", "OE").replace("Ä", "AE");

		//replace special characters
		result = result.replaceAll("[\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\+\\|\\'\\;\\.\\,\\?\\:]", "");
		
		return result;
	}

	/**
	 * Returns the full url for a file entry starting with "http"
	 *
	 * @param themeDisplay
	 *            themedisplay
	 * @param entry
	 *            fileEntry
	 * @return and url to the file
	 */
	private String getFullyQualifiedUrlForFileEntry(ThemeDisplay themeDisplay, FileEntry entry) {
		return themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/"
				+ themeDisplay.getScopeGroupId() + "/" + entry.getFolderId() + "/" + entry.getTitle();
	}

	/**
	 * helper function to get a list of tags as their primary keys
	 *
	 * @return
	 */
	private List<Long> getTagsAsPrimaryKeyList() {
		TagDataLocalService tagLocalService = tagLocalServiceTracker.getService();
		ArrayList<Long> tagsByPrimaryKey = new ArrayList<Long>();
		for (String s : selectedTags) {
			tagsByPrimaryKey.add(tagLocalService.getTagIdByTagName(s));
		}
		return tagsByPrimaryKey;
	}

	public boolean isLoggedIn() {
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
		if (!themeDisplay.isSignedIn()) {
			try {
				externalContext.redirect("/c/portal/login");
			} catch (IOException e) {
				logger.error("No redirect possible" , e);
			}
		}else{
			//make sure everything is set correctly when first loading the modal.
			this.backToStepOne();
			return true;
		}
		
		return false;

	}

	private File convertDataUrlToFile(String dataUrl) throws IOException {
		byte[] imagedata = DatatypeConverter.parseBase64Binary(dataUrl.substring(dataUrl.indexOf(",") + 1));

		InputStream in = new ByteArrayInputStream(imagedata);
		BufferedImage bImageFromConvert = ImageIO.read(in);
		File f = new File("image.png");
		ImageIO.write(bImageFromConvert, "png", f);
		return f;
	}

	public Role getRoleById(final long companyId, final String roleStrId) {
		RoleLocalService roleService = roleLocalServiceTracker.getService();
		try {
			return roleService.getRole(companyId, roleStrId);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public User getAdmin() {
		final long companyId = PortalUtil.getDefaultCompanyId();
		UserLocalService userService = userLocalServiceTracker.getService();
		Role role = null;
		try {
			role = getRoleById(companyId, RoleConstants.ADMINISTRATOR);
			for (final User admin : userService.getRoleUsers(role.getRoleId())) {
				return admin;
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean showPicsOverview(){
		//all images empty
		if(this.titleImgUrl1 == null && this.titleImgUrl2 == null && this.titleImgUrl3 == null){
			return false;
		}
		if(this.titleImgUrl1.equals("/") && this.titleImgUrl2.equals("/") && this.titleImgUrl3.equals("/")){
			return false;
		}		
		return true;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * @param shortDescription
	 *            the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	/**
	 * @return the lat
	 */
	public double getLat() {
		return lat;
	}

	/**
	 * @param lat
	 *            the lat to set
	 */
	public void setLat(double lat) {
		this.lat = lat;
	}

	/**
	 * @return the lng
	 */
	public double getLng() {
		return lng;
	}

	/**
	 * @param lng
	 *            the lng to set
	 */
	public void setLng(double lng) {
		this.lng = lng;
	}

	/**
	 * @return the ideasLocalServiceTracker
	 */
	public IdeaLocalServiceTracker getIdeasLocalServiceTracker() {
		return ideasLocalServiceTracker;
	}

	/**
	 * @param ideasLocalServiceTracker
	 *            the ideasLocalServiceTracker to set
	 */
	public void setIdeasLocalServiceTracker(IdeaLocalServiceTracker ideasLocalServiceTracker) {
		this.ideasLocalServiceTracker = ideasLocalServiceTracker;
	}

	/**
	 * @return the userLocalServiceTracker
	 */
	public UserLocalServiceTracker getUserLocalServiceTracker() {
		return userLocalServiceTracker;
	}

	/**
	 * @param userLocalServiceTracker
	 *            the userLocalServiceTracker to set
	 */
	public void setUserLocalServiceTracker(UserLocalServiceTracker userLocalServiceTracker) {
		this.userLocalServiceTracker = userLocalServiceTracker;
	}

	/**
	 * @return the dlfileEntryLocalServiceTracker
	 */
	public DLFileEntryLocalServiceTracker getDlfileEntryLocalServiceTracker() {
		return dlfileEntryLocalServiceTracker;
	}

	/**
	 * @param dlfileEntryLocalServiceTracker
	 *            the dlfileEntryLocalServiceTracker to set
	 */
	public void setDlfileEntryLocalServiceTracker(DLFileEntryLocalServiceTracker dlfileEntryLocalServiceTracker) {
		this.dlfileEntryLocalServiceTracker = dlfileEntryLocalServiceTracker;
	}

	/**
	 * @return the dlfolderLocalServiceTracker
	 */
	public DLFolderLocalServiceTracker getDlfolderLocalServiceTracker() {
		return dlfolderLocalServiceTracker;
	}

	/**
	 * @param dlfolderLocalServiceTracker
	 *            the dlfolderLocalServiceTracker to set
	 */
	public void setDlfolderLocalServiceTracker(DLFolderLocalServiceTracker dlfolderLocalServiceTracker) {
		this.dlfolderLocalServiceTracker = dlfolderLocalServiceTracker;
	}

	/**
	 * @return the dlAppLocalServiceTracker
	 */
	public DLAppLocalServiceTracker getDlAppLocalServiceTracker() {
		return dlAppLocalServiceTracker;
	}

	/**
	 * @param dlAppLocalServiceTracker
	 *            the dlAppLocalServiceTracker to set
	 */
	public void setDlAppLocalServiceTracker(DLAppLocalServiceTracker dlAppLocalServiceTracker) {
		this.dlAppLocalServiceTracker = dlAppLocalServiceTracker;
	}

	/**
	 * @return the ideasPicture
	 */
	public UploadedFile getIdeasPicture() {
		return ideasPicture;
	}

	/**
	 * @param ideasPicture
	 *            the ideasPicture to set
	 */
	public void setIdeasPicture(UploadedFile ideasPicture) {
		this.ideasPicture = ideasPicture;
	}

	/**
	 * @return the publish
	 */
	public boolean isPublish() {
		return publish;
	}

	/**
	 * @param publish
	 *            the publish to set
	 */
	public void setPublish(boolean publish) {
		this.publish = publish;
	}

	/**
	 * @return the currentIdeasId
	 */
	public long getCurrentIdeasId() {
		return currentIdeasId;
	}

	/**
	 * @param currentIdeasId
	 *            the currentIdeasId to set
	 */
	public void setCurrentIdeasId(long currentIdeasId) {
		this.currentIdeasId = currentIdeasId;
	}

	/**
	 * @return the selectedCategory
	 */
	public String getSelectedCategory() {
		return selectedCategory;
	}

	/**
	 * @param selectedCategory
	 *            the selectedCategory to set
	 */
	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	/**
	 * @return the ideasVideo
	 */
	public UploadedFile getIdeasVideo() {
		return ideasVideo;
	}

	/**
	 * @param ideasVideo
	 *            the ideasVideo to set
	 */
	public void setIdeasVideo(UploadedFile ideasVideo) {
		this.ideasVideo = ideasVideo;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return the goal
	 */
	public String getGoal() {
		return goal;
	}

	/**
	 * @param goal
	 *            the goal to set
	 */
	public void setGoal(String goal) {
		this.goal = goal;
	}

	/**
	 * @return the targetAudience
	 */
	public String getTargetAudience() {
		return targetAudience;
	}

	/**
	 * @param targetAudience
	 *            the targetAudience to set
	 */
	public void setTargetAudience(String targetAudience) {
		this.targetAudience = targetAudience;
	}

	/**
	 * @return the importance
	 */
	public String getImportance() {
		return importance;
	}

	/**
	 * @param importance
	 *            the importance to set
	 */
	public void setImportance(String importance) {
		this.importance = importance;
	}

	/**
	 * @return the solution
	 */
	public String getSolution() {
		return solution;
	}

	/**
	 * @param solution
	 *            the solution to set
	 */
	public void setSolution(String solution) {
		this.solution = solution;
	}

	/**
	 * @return the selectedTags
	 */
	public List<String> getSelectedTags() {
		return selectedTags;
	}

	/**
	 * @param selectedTags
	 *            the selectedTags to set
	 */
	public void setSelectedTags(List<String> selectedTags) {
		this.selectedTags = selectedTags;
	}

	/**
	 * @return the ideasVideoInputStream
	 */
	public InputStream getIdeasVideoInputStream() {
		return ideasVideoInputStream;
	}

	/**
	 * @param ideasVideoInputStream
	 *            the ideasVideoInputStream to set
	 */
	public void setIdeasVideoInputStream(InputStream ideasVideoInputStream) {
		this.ideasVideoInputStream = ideasVideoInputStream;
	}

	/**
	 * @return the dataUrl
	 */
	public String getDataUrl1() {
		return dataUrl1;
	}

	/**
	 * @param dataUrl
	 *            the dataUrl to set
	 */
	public void setDataUrl1(String dataUrl) {
		this.dataUrl1 = dataUrl;
	}

	public String getStyleModalOne() {
		return this.styleModalOne;
	}

	public String getStyleModalTwo() {
		return this.styleModalTwo;
	}

	public String getStyleModalOverview() {
		return this.styleModalOverview;
	}

	public void setStyleModalOne(String styleModalOne) {
		this.styleModalOne = styleModalOne;
	}

	public void setStyleModalTwo(String styleModalTwo) {
		this.styleModalTwo = styleModalTwo;
	}

	public void setStyleModalOverview(String styleModalOver) {
		this.styleModalOverview = styleModalOver;
	}

	/**
	 * @return the pitch
	 */
	public String getPitch() {
		return pitch;
	}

	/**
	 * @param pitch
	 *            the pitch to set
	 */
	public void setPitch(String pitch) {
		this.pitch = pitch;
	}

	/**
	 * @return the titleImgUrl
	 */
	public String getTitleImgUrl1() {
		return titleImgUrl1;
	}

	/**
	 * @param titleImgUrl
	 *            the titleImgUrl to set
	 */
	public void setTitleImgUrl1(String titleImgUrl) {
		this.titleImgUrl1 = titleImgUrl;
	}

	/**
	 * @return the videoPreviewUrl
	 */
	public String getVideoPreviewUrl() {
		return videoPreviewUrl;
	}

	/**
	 * @param videoPreviewUrl
	 *            the videoPreviewUrl to set
	 */
	public void setVideoPreviewUrl(String videoPreviewUrl) {
		this.videoPreviewUrl = videoPreviewUrl;
	}

	/**
	 * @return the styleModalThree
	 */
	public String getStyleModalThree() {
		return styleModalThree;
	}

	/**
	 * @param styleModalThree the styleModalThree to set
	 */
	public void setStyleModalThree(String styleModalThree) {
		this.styleModalThree = styleModalThree;
	}

	/**
	 * @return the selectedUserName
	 */
	public String getSelectedUserName() {
		return selectedUserName;
	}

	/**
	 * @param selectedUserName the selectedUserName to set
	 */
	public void setSelectedUserName(String selectedUserName) {
		this.selectedUserName = selectedUserName;
	}

	/**
	 * @return the possibleUsernames
	 */
	public Map<String,String> getPossibleUsernames() {
		return possibleUsernames;
	}

	/**
	 * @param possibleUsernames the possibleUsernames to set
	 */
	public void setPossibleUsernames(Map<String,String> possibleUsernames) {
		this.possibleUsernames = possibleUsernames;
	}

	/**
	 * @return the dataUrl2
	 */
	public String getDataUrl2() {
		return dataUrl2;
	}
	
	/**
	 * @return the dataUrl3
	 */
	public String getDataUrl3() {
		return dataUrl3;
	}

	/**
	 * @param dataUrl2 the dataUrl2 to set
	 */
	public void setDataUrl2(String dataUrl2) {
		this.dataUrl2 = dataUrl2;
	}
	

	/**
	 * @param dataUrl3 the dataUrl3 to set
	 */
	public void setDataUrl3(String dataUrl3) {
		this.dataUrl3 = dataUrl3;
	}
	
	/**
	 * @return the titleImgUrl2
	 */
	public String getTitleImgUrl2() {
		return titleImgUrl2;
	}
	
	/**
	 * @return the titleImgUrl3
	 */
	public String getTitleImgUrl3() {
		return titleImgUrl3;
	}

	/**
	 * @param titleImgUrl2 the titleImgUrl2 to set
	 */
	public void setTitleImgUrl2(String titleImgUrl2) {
		this.titleImgUrl2 = titleImgUrl2;
	}
	
	/**
	 * @param titleImgUrl3 the titleImgUrl3 to set
	 */
	public void setTitleImgUrl3(String titleImgUrl3) {
		this.titleImgUrl3 = titleImgUrl3;
	}
	
	public boolean isPrivacyNoticeCheckbox() {
		return privacyNoticeCheckbox;
	}

	public void setPrivacyNoticeCheckbox(boolean privacyNoticeCheckbox) {
		this.privacyNoticeCheckbox = privacyNoticeCheckbox;
	}
}
